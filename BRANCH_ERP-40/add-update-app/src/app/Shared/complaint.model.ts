export class Complaint{
	ComplaintId: number;
	Year: number;
	ReportReference: string;
	Domain: number;
	CompanyEmployeeId: string;
	CompanyEmployeeName: string;
	ComplaintType: number;
	ComplaintStatus: number;
	Comment: string;
	EmailReference:string;
	DateCreated: string;
	CreatedBy: string;
	UpdatedBy: string;
	constructor(ComplaintId:number,Year: number,ReportReference: string,Domain: number, 
				CompanyEmployeeId: string, CompanyEmployeeName: string, ComplaintType: number,ComplaintStatus: number,
				comment: string,EmailReference:string){
		this.ComplaintId = ComplaintId;
		this.Year = Year;
		this.ReportReference = ReportReference;
		this.Domain = Domain;
		this.CompanyEmployeeId = CompanyEmployeeId;
		this.CompanyEmployeeName = CompanyEmployeeName;
		this.ComplaintType = ComplaintType;
		this.ComplaintStatus = ComplaintStatus;
		this.Comment = comment;
		this.EmailReference = EmailReference;
	}

}