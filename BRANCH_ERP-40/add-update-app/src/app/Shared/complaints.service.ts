import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Complaint } from './complaint.model';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
@Injectable()
export class ComplaintsService {
private BaseUrl = 'http://audit-test-api.azurewebsites.net/api/complaints';
private EmpUrl = 'http://certiservices.azurewebsites.net/api/companyemployee/complaint';
  constructor(private _http: Http) { }

  /* GET API METHOD */
  getAPI(){
  	return this._http.get(this.BaseUrl)
  	.map((res: Response) => res.json())
  	.catch(this.handleError);
  }
  getEmp(){
    return this._http.get(this.EmpUrl)
  	.map((res: Response) => res.json())
  	.catch(this.handleError);
  }
  handleError(error: Response){
  	console.error(error);
  	return Observable.throw(error);
  }
   /* POST API METHOD */
  saveComplaint(complaint: Complaint): Observable<Complaint>{
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this._http.post(this.BaseUrl, complaint)
    .catch((error:any) => Observable.throw(error.json()
      .error || "Server Error"));
  }
  findById(id: number): Observable<Complaint>{
    return this._http.get(this.BaseUrl+'/'+id)
    .map((res: Response) => res.json())
    .catch((this.handleError));
  }
  updateComplaint(complaint: Complaint): Observable<Complaint>{
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this._http.put(this.BaseUrl, complaint)
    .map((res: Response) => res.json())
    .catch((this.handleError));
  }
  deleteComplainById(id: number) {
    return this._http.delete(this.BaseUrl+'/'+id)
      .map((res:Response) => res.json())
      .catch((this.handleError));
  }
}