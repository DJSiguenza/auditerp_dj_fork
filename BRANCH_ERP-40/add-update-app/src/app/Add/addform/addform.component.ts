import { Component, OnInit } from '@angular/core';
import { ComplaintsService } from '../../Shared/complaints.service';
import { FormControl, FormGroup, Validator, Validators } from '@angular/forms';
import { Complaint } from '../../Shared/complaint.model';
declare var $:any;
@Component({
  selector: 'app-addform',
  templateUrl: './addform.component.html',
  styleUrls: ['./addform.component.css'],
  providers: [ComplaintsService]
})
export class AddformComponent implements OnInit {
  complaint: Complaint;
	complaintForm : FormGroup;
	optionEmp: Array<any>;
	errorMessage: string;
	CompanyEmployeeId: any;

  constructor(private complaintService: ComplaintsService){ }
  DropdownEmp(){
    this.complaintService.getEmp()
  	.subscribe(
  		optionEmp => this.optionEmp = optionEmp,
  			error => this.errorMessage = "Server ERROR!"); 
	}
	change(){
		(<HTMLInputElement>document.getElementById('EmployeeName')).value = $("#CompanyEmp option:selected").text();
	}
  ngOnInit() {
		this.DropdownEmp();
  	this.complaintForm = new FormGroup({
  		Year: new FormControl('',[Validators.required,Validators.minLength(4)]),
  		ReportReference: new FormControl(),
  		Domain: new FormControl(),
			CompanyEmployeeId: new FormControl(),
			CompanyEmployeeName: new FormControl(),
  		ComplaintType: new FormControl(),
			ComplaintStatus: new FormControl(),
			Comment: new FormControl(),
			EmailReference: new FormControl()
	
  	});
  }
  onSubmit(){
  	let complaint: Complaint = new Complaint(null,
  		this.complaintForm.controls['Year'].value,
  		this.complaintForm.controls['ReportReference'].value,
  		this.complaintForm.controls['Domain'].value,
			this.complaintForm.controls['CompanyEmployeeId'].value,
			$("#CompanyEmp option:selected").text(),
  		this.complaintForm.controls['ComplaintType'].value,
  		this.complaintForm.controls['ComplaintStatus'].value,
  		this.complaintForm.controls['Comment'].value,
			this.complaintForm.controls['EmailReference'].value,
			
		);
      
      if(confirm("Are you sure you to create Complain?") == true){
          this.complaintService.saveComplaint(complaint).subscribe();
          alert("Complain has been Created!");
					this.complaintForm.reset();
					window.location.href = "/home";

      }
  }
}
